/*
var dialog = document.createElement("dialog");
dialog.textContent = "This is a dialog";
var button = document.createElement("button");
button.textContent = "Close";
dialog.appendChild(button);
button.addEventListener("click", function() {
  dialog.close()
})
document.body.appendChild(dialog);
dialog.showModal();
*/
var activate_highlight = false;

if (activate_highlight) {
    var elements1 = Array.from(document.getElementsByTagName('input'));
    var elements2 = Array.from(document.getElementsByTagName('select'));
    var elements  = elements1.concat(elements2);
    for (var i = 0, l = elements.length; i < l; i++) {
        var element = elements[i];

        // Button
        var button = $('<button/>', {
            class : 'info',
            id    : 'btn_' + i,
            click : function (event) {
                      alert(this.name);
                      event.preventDefault();
                    },
            name : element.name
        });
        $(element.parentElement).append(button);

        // Text...
        var newElement = document.createElement('span');
        newElement.style.className = 'field-name';
        newElement.style.fontFamily = 'monospace';
        newElement.style.fontSize = '8pt';
        newElement.style.color = '#cc0000';
        newElement.appendChild(document.createTextNode(element.name));
        element.parentElement.appendChild(newElement);
    }

    var elements = document.getElementsByTagName('table');
    for (var i = 0, l = elements.length; i < l; i++) {
        var element = elements[i];
        element.style.width = '100%';
    }
}

//alert('after');