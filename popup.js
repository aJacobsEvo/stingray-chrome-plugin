$(document).ready(function() {
    /*
     * Ticket (Enter key + Go button)
     */
    $("#ticket").keyup(function(event) {
        if (event.which === 13) {
            navigateToTicket();
        }
    });
    $("#ticket-go").click(navigateToTicket);

    /*
     * Branch
     */
    
    // Select...
    chrome.storage.sync.get('branch_repository', function(data) {
        if (data.branch_repository) {
            $("#branch-repository").val(data.branch_repository);
        }
    });
    $("#branch-repository").change(function() {
        chrome.storage.sync.set({
            branch_repository: $("#branch-repository").val()
        })
    });
     
    // Enter key...
    $("#branch").keyup(function(event) {
        if (event.which === 13) {
            navigateToBranch();
        }
    });
    
    // Button...
    $("#branch-go").click(navigateToBranch);
    
    /*
     * Pull Request 
     */

    // Select...
    chrome.storage.sync.get('pull_request_repository', function(data) {
        if (data.pull_request_repository) {
            $("#pull-request-repository").val(data.pull_request_repository);
        }
    });
    $("#pull-request-repository").change(function() {
        chrome.storage.sync.set({
            pull_request_repository: $("#pull-request-repository").val()
        })
    });

    // Enter key...
    $("#pull-request").keyup(function(event) {
        if (event.which === 13) {
            navigateToPullRequest();
        }
    });
    
    // Button...
    $("#pull-request-go").click(navigateToPullRequest);
    
    /*
     * All links (new tab)...
     */
    $('body').on('click', 'a', function(){
        chrome.tabs.create({ url: $(this).attr('href') });
        return false;
    });
});

/*
 *
 */
function navigateToTicket() {
    chrome.tabs.create({
        url : "https://stingray.jira.com/browse/" + $("#ticket").val()
    });
    window.close();
}

/*
 *
 */
function navigateToBranch() {
    let repository = $("#branch-repository").val();
    let branch     = $("#branch").val();
    let url        = (branch) ? "https://bitbucket.org/stingray-ondemand/" + repository + "/branch/" + branch
                              : "https://bitbucket.org/stingray-ondemand/" + repository + "/branches/";
    chrome.tabs.create({ url : url });
    window.close();
}

/*
 *
 */
function navigateToPullRequest() {
    let repository   = $("#pull-request-repository").val();
    let pull_request = $("#pull-request").val();
    chrome.tabs.create({
        url : "https://bitbucket.org/stingray-ondemand/" + repository + "/pull-requests/" + pull_request
    });
    window.close();
}